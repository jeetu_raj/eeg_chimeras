import scipy.io 
import glob
from pprint import pprint
import numpy as np
from sklearn.decomposition import PCA
import copy
from sklearn.cross_validation import StratifiedKFold
from sklearn import svm
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.linear_model import ElasticNet
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt

seed = 21

file_list = glob.glob("Data/*.mat")

data = [];

print len(file_list)

del_list = [];
for find in range(0,len(file_list)): 
	data += [scipy.io.loadmat(file_list[find])]
	del data[find]['__header__']
	del data[find]['__globals__']
	del data[find]['__version__']
	del data[find]['ECI_TCPIP_55513']
	print data[find].keys()
	(r,c) = np.matrix(data[find][data[find].keys()[0]]).shape
	if(c>350):
		del_list += [find]

for each in del_list:
	del data[each]

# print len(data)

# print file_list
# pprint(data[0])

# each data set has 129 rows and 350 samples in general except one instance 
# now each of the matrices have been stripped to 4 different matrices -- most likely
# each of the 4 represent the different types of images
# task - see each electrode and check how well we are able to predict the category of the image 
# 4 classes of images : 13 samples each dimension 350 each -- 
# task is to predict the classes and seeing which of the 129 are the best in predicting

def read_lab_coord(filename):
	file = open(filename);
	data = file.read();
	file.close();
	data = data.strip().split("\n");
	coord_dict = {};
	for elem in data:
		info = elem.strip().split(" ")
		coord_dict[int(info[0])] = (int(info[1]),int(info[2]))
	return coord_dict;

lab_coord = read_lab_coord("label_coordinates");

def display():
	im = plt.imread("Data/EEG.png")
	plt.ion()
	fig, ax = plt.subplots()
	plt.imshow(im)
	for i in range(50):
	    y = np.random.random()*1000
	    scat = ax.scatter(i*75, y, c='r', s=40)			#replace with the exact plots obtained from running window
	    plt.pause(50)
	    xy = np.delete(scat.get_offsets(), 0, axis=0)
	    scat.set_offsets(xy)

def select_electrode(data,e_id,img_id):
	X = [];
	Y = [];
	for elem in range(0,len(data)):
		for key in data[elem].keys():
			type = int(key[-1])
			if(type==img_id):
				Y += [1]
				X += [data[elem][key][e_id]]
				# Y += [1]
				# X += [data[elem][key][e_id]]
			elif(type != 3):						#change it when the classification task is suitable.
				Y += [0]
				X += [data[elem][key][e_id]]
			# elif(type==1):
			# 	Y += [0]
			# 	X += [data[elem][key][e_id]]
			# else:
			# 	continue;
	return (X,Y)

def dimen_reduction_PCA(X,Y,dim):
	pca = PCA(n_components=dim);
	pca.fit(X);
	P =  pca.transform(X)
	# print "shape of matrix",P.shape
	return (P,Y);

def dimen_reduction_Elastic(Xt,Yt):
	X = copy.deepcopy(Xt);
	Y = copy.deepcopy(Yt);

	Y = np.array(Y);
	c,r = Y.shape;
	Y = Y.reshape(r,1);
	Y = Y.ravel();
	#also try lasso
	enet = ElasticNet(alpha=0.05, l1_ratio=0.07,positive=True);	#can use ElasticNetCV to automatically decide the alpha values for the dataset

	enet.fit(X,Y);
	weights = enet.coef_;
	nzeroCols = []; 
	for i in range(0,len(weights)):
		if(weights[i]>0):
			nzeroCols += [i];
	Xtemp = X[:,nzeroCols];
	Xgram = np.matmul(Xtemp,np.transpose(Xtemp));			#following first paper doing a grammian reduction
	return (Xtemp,Yt)	


def machine_learning_SVM_strat(Xt,Yt):
	global seed;
	X = copy.deepcopy(Xt);
	Y = copy.deepcopy(Yt);
	C = 1.0
	kfold = StratifiedKFold(np.array(Yt)[0],n_folds=5, shuffle=True, random_state=seed)
	cvscores = []
	r,c = X.shape;
	Y = np.array(Y)[0]

	for train, test in kfold:
		rt,ct = X[train].shape
		train_label = np.array(Y[train]).reshape(rt,1).ravel();
		# print train_label
		svc = svm.SVC(kernel='linear',C=C);
		# svc = svm.SVC(kernel='rbf',C=C);
		svc.fit(X[train],train_label)
		predicted = svc.predict(X[test])
		# print("predicted -",predicted)
		# print("actualpred-",Y[test])
		cvscores = cvscores + [accuracy_score(Y[test],predicted)]
	print cvscores
	return sum(cvscores)/float(len(cvscores));


def machine_learning_Dtree_strat(Xt,Yt):
	global seed;
	X = copy.deepcopy(Xt);
	Y = copy.deepcopy(Yt);
	C = 1.0
	kfold = StratifiedKFold(np.array(Yt)[0],n_folds=5, shuffle=True, random_state=seed)
	cvscores = []
	r,c = X.shape;
	Y = np.array(Y)[0]

	for train, test in kfold:
		rt,ct = X[train].shape
		train_label = np.array(Y[train]).reshape(rt,1).ravel();
		# print train_label
		svc = svm.SVC(kernel='linear',C=C);
		dtree = DecisionTreeClassifier(max_depth=7,random_state=seed)

		# svc = svm.SVC(kernel='rbf',C=C);
		dtree.fit(X[train],train_label)
		predicted = dtree.predict(X[test])
		# print("predicted -",predicted)
		# print("actualpred-",Y[test])
		cvscores = cvscores + [accuracy_score(Y[test],predicted)]
	# print cvscores
	return sum(cvscores)/float(len(cvscores));


def rank_electrodes(featImp):
	tupleList = [];
	for i in range(0,len(featImp)):
		tupleList += [(i+1,featImp[i])]
	tupleList.sort(key=lambda x: x[1])
	tupleList.reverse()
	# pprint(tupleList);
	return tupleList;

def select_window(start,window,Xt):
	if(start+window>Xt.shape[1]):
		return [];
	col_list = []
	for i in range(start,start+window):
		col_list += [i]
	return Xt[:,col_list]


def sliding_window_ranking(data,window,img_id):
	im = plt.imread("Data/EEG.png")
	plt.ion()
	fig, ax = plt.subplots()
	plt.imshow(im)
	samples  = 350;
	for start in range(0,samples-window+1,2):
		acc_list = []
		for i in xrange(128):
			(X,Y) = select_electrode(data,i,img_id);
			(X,Y) = (np.matrix(X),np.matrix(Y))
			X_red  = np.matrix(select_window(start,window,X))
			# print X.shape
			# print X_red.shape
			accuracy = machine_learning_Dtree_strat(X_red,Y)
			acc_list += [accuracy]
		ranks = rank_electrodes(acc_list)
		print "window :",start
		active_elec = [];
		dispX = []
		dispY = []
		for j in range(10):
			active_elec += [ranks[j][1]]
			if(ranks[j][0] in lab_coord.keys()):
				dispX += [lab_coord[ranks[j][0]][0]]
				dispY += [lab_coord[ranks[j][0]][1]]
		scat = ax.scatter(dispX, dispY, c='r', s=40)
		plt.pause(1)
		xy = np.delete(scat.get_offsets(),range(len(dispX)), axis=0)
		scat.set_offsets(xy)
		pprint(active_elec);


sliding_window_ranking(data,10,4)


# avg = 0.0;
# tup_list = []
# for i in xrange(128):
# 	(X,Y) = select_electrode(data,i,4);
# 	# print np.matrix(X).shape
# 	(X_red,Y_red) = dimen_reduction_PCA(np.matrix(X),np.matrix(Y),20)
# 	# (X_red,Y_red) = dimen_reduction_Elastic(np.matrix(X),np.matrix(Y))
# 	print X_red.shape
# 	# accuracy = machine_learning_SVM_strat(X_red,Y_red)
# 	accuracy = machine_learning_Dtree_strat(X_red,Y_red)
# 	tup_list += [accuracy]
# 	# avg += accuracy
# 	# print i,"img_id",1,machine_learning_SVM_strat(X_red,Y_red)

# rank_electrodes(tup_list)



















